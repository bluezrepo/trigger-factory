/**
 * @author Tomas Hnizdil
 * @company bluez.io
 * @date 14.10.2021
 *
 * @description Test class to test AbstractTriggerHandler class
 */
@IsTest
private class AbstractTriggerHandlerTest {

    @IsTest
    private static void testRunOnce() {
        AbstractTriggerHandler handler = new AbstractTriggerHandler();

        Boolean runOnce = handler.runOnce;
        Boolean runTwice = handler.runOnce;
        System.assertEquals(true, runOnce, 'Invalid runOnce value set for the first attempt.');
        System.assertEquals(false, runTwice, 'Invalid runOnce value set for the second attempt.');
    }

    @IsTest
    private static void testByPassTrigger_Default() {
        Boolean bypassAllTriggers = AbstractTriggerHandler.bypassAllTriggers;
        System.assertEquals(false, bypassAllTriggers, 'Invalid bypassTrigger value set.');
    }

    @IsTest
    private static void coverVirtualMethods() {
        AbstractTriggerHandler handler = new AbstractTriggerHandler();
        handler.create(null, null);
        handler.beforeInsert();
        handler.beforeUpdate();
        handler.beforeDelete();
        handler.afterInsert();
        handler.afterUpdate();
        handler.afterDelete();
        handler.afterUndelete();
        // Nothing to assert, just adding code coverage
    }
}