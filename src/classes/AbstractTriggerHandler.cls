/**
 * @author Tomas Hnizdil
 * @company bluez.io
 * @date 14.10.2021
 *
 * @description Utility methods for Triggers.
 */
public virtual with sharing class AbstractTriggerHandler {

    /**
     * @description The name of the currently running trigger handler, retrieved from "this" instance, if needed
     */
    private String triggerName {
        get {
            if (triggerName == null) {
                String fullTriggerInstance = this.toString();
                triggerName = fullTriggerInstance.left(fullTriggerInstance.indexOf(':'));
            }

            return triggerName;
        }
        set;
    }

    /**
     * @description Logic to prevent running a specific trigger logic more than once.
     *              Usage: In the inheriting trigger handler, use the following
     *              if (runOnce) {
     *                  ... // the logic to only run once
     *              }
     */
    private static Set<String> triggersRun = new Set<String>();
    public Boolean runOnce {
        get {
            if (!triggersRun.contains(triggerName)) {
                triggersRun.add(triggerName);
                return true;
            }

            return false;
        }
    }

    /**
     * @description Virtual constructor-like method to set (casted) Trigger.new and Trigger.oldMap data
     *              in the inheriting trigger handler.
     *
     * @param newData Trigger.new data
     * @param oldDataMap Trigger.oldMap data
     */
    public virtual void create(List<SObject> newData, Map<Id, SObject> oldDataMap) {}

    /**
     * @description Virtual methods handling all trigger events.
     *              A particular method (or methods) need to be implemented in the inheriting trigger handler.
     */
    public virtual void beforeInsert() {}
    public virtual void beforeUpdate() {}
    public virtual void beforeDelete() {}

    public virtual void afterInsert() {}
    public virtual void afterUpdate() {}
    public virtual void afterDelete() {}
    public virtual void afterUndelete() {}

    /**
     * @description Logic allowing to bypass all triggers, when desired in unit tests.
     *              Usage: In the unit test, use the following
     *              AbstractTriggerHandler.bypassAllTriggers = true; // to turn all triggers off
     *              AbstractTriggerHandler.bypassAllTriggers = false; // to turn all triggers on
     */
    public static Boolean bypassAllTriggers = false;

    /**
     * @description Logic allowing to bypass specific triggers only, when desired in unit tests.
     *              Usage: In the unit test, use the following
     *              AbstractTriggerHandler.bypassTriggers.add(ContactTriggerHandler.class); // to turn ContactTriggerHandler off
     *              AbstractTriggerHandler.bypassTriggers.remove(ContactTriggerHandler.class); // to turn ContactTriggerHandler on
     */
    public static Set<System.Type> bypassTriggers = new Set<System.Type>();
}