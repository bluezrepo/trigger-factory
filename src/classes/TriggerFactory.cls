/**
 * @author Tomas Hnizdil
 * @company bluez.io
 * @date 14.10.2021
 *
 * @description Used to instantiate and execute Trigger Handlers associated with sObjects.
 */
public with sharing class TriggerFactory {

    @TestVisible
    private static final String EXCEPTION_MESSAGE = 'No Trigger Handler registered for Class Type: ';

    /**
     * @description Public static method to create and execute a trigger handler
     *
     * @param classType Class type to process
     *
     * Throws a TriggerIntException if no handler has been coded.
     *
     * @return True if the trigger logic was executed, false if the trigger logic was bypassed. (For testing purposes only.)
     */
    public static Boolean createHandler(System.Type classType) {
        // Get a handler of the object being processed
        AbstractTriggerHandler handler = getHandler(classType);

        // Make sure we have a handler registered, new handlers must be registered in the getHandler method.
        if (handler == null) {
            throw new TriggerHandlerException(EXCEPTION_MESSAGE + classType);
        }

        // if trigger disabled, exit
        if (AbstractTriggerHandler.bypassAllTriggers || AbstractTriggerHandler.bypassTriggers.contains(classType)) {
            return false;
        }

        // Execute the handler to fulfil the trigger
        execute(handler);
        return true;
    }

    /**
     * @description private static method to get the appropriate handler for the object type.
     * Modify this method to add any additional handlers.
     *
     * @param obType Class to locate
     *
     * @return TriggerHandlerBaseInt - A trigger handler if one exists or null.
     */
    @TestVisible
    private static AbstractTriggerHandler getHandler(System.Type obType) {
        if (obType == null) {
            return null;
        }
        AbstractTriggerHandler handler = (AbstractTriggerHandler) obType.newInstance();
        handler.create(Trigger.new, Trigger.oldMap);
        return handler;
    }

    /**
     * @description private static method to control the execution of the handler
     *
     * @param handler A Trigger Handler to execute
     */
    @TestVisible
    private static void execute(AbstractTriggerHandler handler) {
        if (Trigger.IsBefore) { // Before trigger logic
            if (Trigger.isInsert) {
                handler.beforeInsert();
            } else if (Trigger.isUpdate) {
                handler.beforeUpdate();
            } else if (Trigger.IsDelete) {
                handler.beforeDelete();
            }
        } else if (Trigger.IsAfter) { // After trigger logic
            if (Trigger.IsInsert) {
                handler.afterInsert();
            } else if (Trigger.IsUpdate) {
                handler.afterUpdate();
            } else if (Trigger.IsDelete) {
                handler.afterDelete();
            } else if (Trigger.isUndelete) {
                handler.afterUndelete();
            }
        }
    }

    public class TriggerHandlerException extends Exception {}
}