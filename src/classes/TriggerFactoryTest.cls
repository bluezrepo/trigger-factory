/**
 * @author Tomas Hnizdil
 * @company bluez.io
 * @date 14.10.2021
 *
 * @description Test class for TriggerFactory class
 */
@IsTest
private class TriggerFactoryTest {

    // include GenericStub or any other StubProvider which exists in the org
    private static GenericStub stub;
    private static AbstractTriggerHandler handler;
    static {
        stub = new GenericStub();
        handler = (AbstractTriggerHandler) Test.createStub(AbstractTriggerHandler.class, stub);
    }

    @IsTest
    private static void testCreateHandler_NoHandler() {

        Test.startTest();
        try {
            TriggerFactory.createHandler(null);
            System.assert(false, 'TriggerHandlerException was expected.');
        } catch (TriggerFactory.TriggerHandlerException ex) {
            System.assertEquals(TriggerFactory.EXCEPTION_MESSAGE + 'null', ex.getMessage(), 'Invalid message returned.');
        }
        Test.stopTest();
    }

    @IsTest
    private static void testCreateHandler_Bypass() {
        AbstractTriggerHandler.bypassAllTriggers = true;

        Test.startTest();
        Boolean result = TriggerFactory.createHandler(AbstractTriggerHandler.class);
        Test.stopTest();

        System.assertEquals(false, result, 'Trigger should have been bypassed.');
    }

    @IsTest
    private static void testGetHandler() {
        Test.startTest();
        // replace "ContactTriggerHandler" with any TriggerHandler class which exists in the org
        AbstractTriggerHandler result = TriggerFactory.getHandler(ContactTriggerHandler.class);
        Test.stopTest();

        System.assert(result instanceof ContactTriggerHandler, 'Invalid trigger handler returned.');
    }
}