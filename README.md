## Usage

Create a trigger and a trigger handler class on a particular object in following way.

### The trigger

```
trigger ContactTrigger on Contact (after update, before update) {
    TriggerFactory.createHandler(ContactTriggerHandler.class);
}
```

### The trigger handler

```
public with sharing class ContactTriggerHandler extends AbstractTriggerHandler {

	// Service class to hold the actual logic
    @TestVisible
    private static ContactService service {
        get {
            if (service == null) {
                service = new ContactService();
            }
            return service;
        }
        set;
    }
    private List<Contact> newData;
    private Map<Id, Contact> oldDataMap;

    /**
     * @description Constructor-like method, defined in the AbstractTriggerHandler and called from TriggerFactory,
     *              passing Trigger.new and Trigger.oldMap data from the trigger context.
     *              (Standard constructor cannot be used because apex doesn't support invoking dynamic classes
     *               with newInstance() method and passing parameters in it. And we want to avoid calling
     *               the Trigger context explicitly here.)
     *
     * @param newData Trigger.new data from the trigger context
     * @param oldDataMap Trigger.oldMap data from the trigger context
     */
    public override void create(List<SObject> newData, Map<Id, SObject> oldDataMap) {
        this.newData = (List<Contact>) newData;
        this.oldDataMap = (Map<Id, Contact>) oldDataMap;
    }

    /**
     * @description Method handling before insert trigger event. Called from TriggerFactory, no need to call explicitly.
     */
    public override void beforeInsert() {
        service.serviceMethodToCallBeforeInsert(newData, oldDataMap);
    }

    /**
     * @description Method handling before update trigger event. Called from TriggerFactory, no need to call explicitly.
     */
    public override void beforeUpdate() {
        service.serviceMethodToCallBeforeUpdate(newData, oldData);
    }
}
```